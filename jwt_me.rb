require 'jwt'

class JwtMe
  attr_accessor :token, :hmac_secret
  
    
  def initialize(input_stream, output_stream)
    @input_stream = input_stream
    @output_stream = output_stream
  end

  def run
    @payload = {}

    @output_stream.puts 'Starting with JWT token generation'
    done_entering = false
    until @payload["user_id"] && @payload["email"] && done_entering
      if !@payload["user_id"]
        prompt_for "user_id"
      elsif !@payload["email"]
        prompt_for "email"
      else
        done_entering = !prompt_for_more
      end
    end
    if @hmac_secret
      @token = JWT.encode @payload, @hmac_secret, 'HS256'
    else
      @token = JWT.encode @payload, nil, "none"
    end
    begin
      IO.popen('pbcopy', 'w') { |f| f << @token }
    rescue Exception
    end
    begin
      IO.popen('clip', 'w') { |f| f << @token }
    rescue Exception
    end
    @output_stream.puts "the JWT has been copied to your Clipboard."
  end
  
  private
  
  def valid_email?(email)
    email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  end
  
  def prompt_for(var_name)
    @output_stream.puts "Enter #{var_name}"
    val = @input_stream.gets.chomp
    valid, err_msg = validate(var_name, val)
    if valid
      @payload[var_name] = val
    else
      @output_stream.puts err_msg
    end
  end
  
  def validate(var_name, value)
    if var_name.to_s == "user_id" && value.length < 2
      err_msg = "Invalid user_id - value must be longer than one character"
    end
    if var_name.to_s == "email" && !valid_email?(value)
      err_msg = "Invalid email format"
    end
    return (err_msg == nil), err_msg
  end
  
  def prompt_for_more
    @output_stream.puts "More key/value pairs to enter? (Y or N)"
    yes_or_no = @input_stream.gets.chomp
    if yes_or_no == "Y"
      key = ""
      until key.length > 0
        puts "Key name?"
        key = @input_stream.gets.chomp
      end
      prompt_for key
      return true
    else
      return false
    end
  end
  
end

if $0 == __FILE__
  jwt_me = JwtMe.new(STDIN, STDOUT)
  jwt_me.hmac_secret = ENV["JWT_GEN_HMAC_SECRET"]
  jwt_me.run
end
