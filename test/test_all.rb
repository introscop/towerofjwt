require 'minitest/autorun'
require 'jwt'
require_relative '../jwt_me.rb'

class TestAll < Minitest::Test
  def test_proper_input
    input = StringIO.new
    input.puts "user1"
    input.puts "alex@io.com"
    input.puts "N"
    input.rewind
    output = StringIO.new
    jwt_me = JwtMe.new(input, output)
    jwt_me.run
    assert_match /Starting with JWT token generation/, output.string
    assert_match /the JWT has been copied/, output.string
    assert jwt_me.token
    decoded_token = JWT.decode jwt_me.token, nil, false
    assert_match /user1/, decoded_token.to_s
  end
  
  def test_bad_input
    input = StringIO.new
    input.puts "user1"
    input.puts "bademail"
    input.puts "alex@io.com"
    input.puts "N"
    input.rewind
    output = StringIO.new
    jwt_me = JwtMe.new(input, output)
    jwt_me.run 
    assert_match /Invalid email format/, output.string
  end
  
  def test_hmac_secret
    input = StringIO.new
    input.puts "user1"
    input.puts "alex@io.com"
    input.puts "N"
    input.rewind
    output = StringIO.new
    jwt_me = JwtMe.new(input, output)
    jwt_me.hmac_secret = "mysecretkey"
    jwt_me.run
    assert_match /Starting with JWT token generation/, output.string
    assert_match /the JWT has been copied/, output.string
    assert jwt_me.token
    decoded_token = JWT.decode jwt_me.token, jwt_me.hmac_secret, true, { :algorithm => 'HS256' }
    assert_match /user1/, decoded_token.to_s
  end
end